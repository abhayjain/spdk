// Copyright 2017, Tintri, Inc. All rights reserved.

#include <fileops/GenericRequest.h>
#include <fileops/FileOperations.h>
#include <fileops/FileOperationSplitterBasicTypes.h>
#include <recovery/Recovery.h>
#include <qos/QosEngine.h>

class IscsiRequestThreadPoolEntry;

class IscsiRequest : public GenericRequest {

    // Prevent the compiler from generating a default
    // copy constructor and assignment operator
    DISABLE_COPY_AND_ASSIGN( IscsiRequest );

    GenericRequestType type_;
    byte *opBuf_;
    Ptr<InMemoryFileMetadata> imFile_;
    Principal principal_;
    Ptr<VmFileSetStats> fileSetStats_;
    int retCode_;

    ThreadPoolEntry *tpe_;
    void *bdevDriverCtx_; // the context spdk bdev needs to return.

public:

    // XXX opBuf must be allocated already.
    IscsiRequest( byte *opBuf,
                  uint64 ioSize,
                  uint64 ioOffset,
                  Ptr<InMemoryFileMetadata> imFile,
                  Principal principal,
                  Ptr<VmFileSetStats> fileSetStats,
                  GenericRequestType type,
                  void *bdevDriverCtx_ );

    virtual ~IscsiRequest() {
        tpe_ = NULL;
    }

    GET_AND_SET_ACCESSORS( byte*, opBuf );
    GET_AND_SET_ACCESSORS( Ptr<InMemoryFileMetadata>, imFile );
    GET_AND_SET_ACCESSORS( Principal, principal );
    GET_AND_SET_ACCESSORS( Ptr<VmFileSetStats>, fileSetStats );
    GET_AND_SET_ACCESSORS( int, retCode );
    GET_AND_SET_ACCESSORS( GenericRequestType, type );

    FileAttr* fileAttPtr() {
        return &fileAttr_;
    }

    virtual void preEnqueue() {
        latencyStat_.admitQNs_ = getTimeNs();
    }

    virtual void postEnqueue() {
        latencyStat_.enqueueDoneNs_ = getTimeNs();
    }

    virtual void enqueueReply( int errorCode ) {
        if ( errorCode != errOk ) {
            switch ( errorCode ) {
                case errFileSystemFull:
                    retCode_= -ENOSPC;
                    break;
                default:
                    // Pass the error code back to the user.
                    // SPDK_BDEV_IO_STATUS_FAILED ?
                    panic( "Unknown error on request %d", errorCode );
            }
        }

        iscsiRequestExecutionComplete();
    }

    void iscsiRequestExecutionComplete( void ) {
        bdev_realstore_io_complete( bdevDriverCtx_, retCode_ );
    }

    bool prepareUserOperation( CreateOpArgs *args ) {
        return false;
    }

    virtual ErrorReturn prepareUserOperation( SyncReplWriteOpArgs *args ) {
        return errorReturn( errOk );
    }

    SyncReplServerToken getServerToken() {
        return DEFAULT_IP_TOKEN;
    }

    void reqAcquiredBarrier() { 
        //GenericRequest::hasBarrier( true );
    }
    void qosEnqueue() {
        realstoreQos->enqueue( this );
    }
};

class IscsiRequestThreadPoolEntry : public GenericThreadPoolEntry {
    Ptr<IscsiRequest> req_;
public:

    IscsiRequestThreadPoolEntry( Ptr<IscsiRequest> req ) : req_( req ) {
        FileGlobalId fileGlobalId = req_->imFile()->fileGlobalId();
        FileHandle handle( FH_TYPE_REALSTORE_PERSISTENT, fileGlobalId.fileId(), 0 );
        UUID uuid = fileGlobalId.uuid();
        req_->GenericRequest::initialize( req_->type(),
                                          dynamic_cast<GenericThreadPoolEntry*>( this ),
                                          &uuid,
                                          &handle, 
                                          req_->ioSize(),
                                          req_->ioOffset(),
                                          req_->imFile()->createTime() );
    }

    void threadPoolFunc( ThreadPoolBase *threadPool, uint64 threadNum ) {
       req_->latencyStat_.waitNs_ = getTimeNs();
       int ret = 0;
       switch( req_->type() ) {
           case GENERIC_REQUEST_VFS_REALSTORE_WRITE:
            {
                assert( req_->imFile() );

                ret = realstoreFileWrite( req_->imFile(),
                                          ( char* )req_->opBuf(),
                                          req_->ioSize(),
                                          req_->ioOffset(),
                                          req_->fileSetStats(),
                                          req_->fileAttPtr(),
                                          req_->principal() );
                req_->retCode( ret );
                req_->invokeQosCallback();
                break;
            }
           case GENERIC_REQUEST_VFS_REALSTORE_READ:
            {
                ret = realstoreFileRead( req_->imFile(),
                                         ( char * )req_->opBuf(),
                                         req_->ioSize(),
                                         req_->ioOffset(),
                                         req_->fileSetStats(),
                                         req_->fileAttPtr(),
                                         req_->principal() );
                req_->retCode( ret );
                req_->invokeQosCallback();
                break;
            }
           default:
                panic( "Operation type not supported" );
       }

       req_->latencyStat_.nfsNs_= getTimeNs();

        ErrorReturn err;
        if ( ret < 0 ) {
            switch( ret ) {
                case -ENOSPC:
                    err = errorReturn( errFileSystemFull );
                    break;
                case -EFBIG:
                    err = errorReturn( errTooBig );
                    break;
                default :
                    err = errorReturn( errInvalidArgument );
                    break;
            }
       }

       req_->enqueueReply( err.errorCode() );

    }
};

IscsiRequest::IscsiRequest( byte *opBuf, uint64 ioSize, uint64 ioOffset, Ptr<InMemoryFileMetadata> imFile, Principal principal, Ptr<VmFileSetStats> fileSetStats, GenericRequestType type, void *bdevDriverCtx )
            : type_( type ), opBuf_( opBuf ), imFile_( imFile ), principal_( principal ),
              fileSetStats_( fileSetStats ), bdevDriverCtx_( bdevDriverCtx_ ) {
    switch( type ) {
        case GENERIC_REQUEST_VFS_REALSTORE_WRITE:
        {
            fileAttr_.fileId_ = imFile->fileId();
            ioOffset_ = ioOffset;
            ioSize_ = ioSize;
            type_ = GENERIC_REQUEST_VFS_REALSTORE_WRITE;
            break;
        }
        case GENERIC_REQUEST_VFS_REALSTORE_READ:
        {
            fileAttr_.fileId_ = imFile->fileId();
            ioOffset_ = ioOffset;
            ioSize_ = ioSize;
            type_ = GENERIC_REQUEST_VFS_REALSTORE_READ;
            break;
        }
        default:
        {
            panic( "IscsiRequest Does Not Support %d request type", type );
        }
    }

    imFile_->fileAttr( &fileAttr_ );
    tpe_ = new IscsiRequestThreadPoolEntry( this );
}

int realstoreIscsiWriteFile( FileId fileId,
                             void *buf,
                             size_t size,
                             off_t offset,
                             void *bdevDriverCtx ) {
    Ptr<InMemoryFileMetadata> imFile = realstoreFileMetadataManager->file( fileId );
    Principal principal( PRINCIPAL_MISC );

    Ptr<IscsiRequest> req = new IscsiRequest( (byte*) buf,
                                              size,
                                              offset,
                                              imFile,
                                              principal,
                                              NULL,
                                              GENERIC_REQUEST_VFS_REALSTORE_WRITE,
                                              bdevDriverCtx );

    req->latencyStat_.enqueueStartNs_ = getTimeNs();
    
    req->qosEnqueue();

    return 0;
}

int realstoreIscsiReadFile( FileId fileId,
                            void *buf,
                            size_t size,
                            off_t offset,
                            void *bdevDriverCtx ) {
    Ptr<InMemoryFileMetadata> imFile = realstoreFileMetadataManager->file( fileId );
    Principal principal( PRINCIPAL_MISC );

    Ptr<IscsiRequest> req = new IscsiRequest( (byte*) buf,
                                              size,
                                              offset,
                                              imFile,
                                              principal,
                                              NULL,
                                              GENERIC_REQUEST_VFS_REALSTORE_READ,
                                              bdevDriverCtx );

    req->latencyStat_.enqueueStartNs_ = getTimeNs();
    
    req->qosEnqueue();

    return 0;
}

int realstoreIscsiCreateFile( char *fileName, FileId *fileId_p ) {
    int ret = 0;
    FileAttr fileAttr;
    Ptr<InMemoryFileMetadata> parentDir;
    Ptr<InMemoryFileMetadata> imFile_;

    String filePath( fileName );

    bool found = realstoreDirectory->lookupParentDir( filePath,
                                                      parentDir );
    assert( found );
    FileId parentDirFid = parentDir->fileId();
    String deleteCallerName = "IscsiDevice";
    ret = realstoreFileDelete( parentDirFid,
                               filePath.c_str(),
                               deleteCallerName.c_str() );

    if ( ret < 0 ) {
        return ret;
    }
    ret = realstoreFileCreate( parentDirFid,
                               filePath.c_str(),
                               FileType::file(),
                               Permissions( 0644 ),
                               UserId( 0 ),
                               GroupId( 0 ),
                               "",
                               NULL, // replInfo
                               &fileAttr );

    if ( ret < 0 ) {
        return ret;
    }
    found = realstoreDirectory->lookupFile( filePath, imFile_ );
    assert( found );

    *fileId_p = imFile_->fileId();
    return 0;
}
