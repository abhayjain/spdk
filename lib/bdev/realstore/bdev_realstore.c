/*-
 *   BSD LICENSE
 *
 *   Copyright (c) Intel Corporation.
 *   All rights reserved.
 *
 *   Redistribution and use in source and binary forms, with or without
 *   modification, are permitted provided that the following conditions
 *   are met:
 *
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in
 *       the documentation and/or other materials provided with the
 *       distribution.
 *     * Neither the name of Intel Corporation nor the names of its
 *       contributors may be used to endorse or promote products derived
 *       from this software without specific prior written permission.
 *
 *   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *   "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *   LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *   A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 *   OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 *   SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 *   LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *   DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *   THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *   (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 *   OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */


#include "spdk/stdinc.h"

#include "spdk/event.h"
#include "spdk/bdev.h"
#include "spdk/conf.h"
#include "spdk/env.h"
#include "spdk/fd.h"
#include "spdk/io_channel.h"
#include "spdk/json.h"
#include "spdk/util.h"
#include "spdk_internal/bdev.h"

#include "spdk_internal/log.h"

#define REALSTORE_DIR_PATH "./iscsi/"
#define REALSTORE_DEFAULT_FILE_SIZE ( 1ul * 1024 * 1024 * 1024 )

/*
 * Realstore backend registered for every lun with bdev layer.
 */
struct realstore_bdev {
    struct spdk_bdev    bdev;
    char                *filename;
    uint64_t            fileid;
    TAILQ_ENTRY(realstore_bdev)  link;
};

/*
 * For every async io, pass this struct allocated as part of bdev_io->driver_ctx
 * to the underlying pipeline. On completion of the io in the async callback,
 * this can be used to get to bdev_aio struct that serves as the parameter
 * to the spdk_bdev_io_complete().
 */
struct bdev_realstore_task {
    uint32_t            lcore;
    int                 io_status;
    TAILQ_ENTRY(bdev_realstore_task)    link;
};
static TAILQ_HEAD(, realstore_bdev) g_realstore_bdev_head;

static int bdev_realstore_initialize(void);
static void realstore_free_file(struct realstore_bdev *rsbev);
static void bdev_realstore_get_spdk_running_config(FILE *fp);
struct spdk_bdev * create_realstore_file(const char *id, const char *filename, uint32_t block_size);

int realstoreIscsiCreateFile( char *fileName, uint64_t *fileId_p );
int realstoreIscsiWriteFile( uint64_t fileId,
                             void *buf,
                             size_t size,
                             off_t offset,
                             void *bdevDriverCtx ) ;
int realstoreIscsiReadFile( uint64_t fileId,
                            void *buf,
                            size_t size,
                            off_t offset,
                            void *bdevDriverCtx );
int realstoreIscsiCreateFile( char *fileName, uint64_t *fileId_p ) {
    return 0;
}
int realstoreIscsiWriteFile( uint64_t fileId,
                             void *buf,
                             size_t size,
                             off_t offset,
                             void *bdevDriverCtx ) {
    return 0;
}

int realstoreIscsiReadFile( uint64_t fileId,
                            void *buf,
                            size_t size,
                            off_t offset,
                            void *bdevDriverCtx ) {
    return 0;
}
/*
 * Retrieves ctx size to be used while allocating bdev_io struct for an pending IO.
 */
static int
bdev_realstore_get_ctx_size(void)
{
    return sizeof(struct bdev_realstore_task);
}

SPDK_BDEV_MODULE_REGISTER(realstore,    // _name
                          bdev_realstore_initialize, // init_fn
                          NULL, // fini_fn
                          bdev_realstore_get_spdk_running_config, // config_fn
                          bdev_realstore_get_ctx_size, // ctx_size_fn
                          NULL) // examine_fn

/*
 * Creates realstore backed file for this bdev.
 */
static int
bdev_realstore_create(struct realstore_bdev *rsbdev)
{
    char full_file_name[100];
    uint64_t fileid;
    int rc;

    /* create full_file_name that goes as iscsi/<filename> */
    snprintf(full_file_name, sizeof(full_file_name), "%s%s", REALSTORE_DIR_PATH,
                                                             rsbdev->filename);

    rc = realstoreIscsiCreateFile(full_file_name, &fileid);

    if (rc < 0) {
        return -1;
    }

    rsbdev->fileid = fileid;

    return 0;
}

/*
 * Following prepares a read request to the underlying
 * realstore file engine. Returns immediately after
 * enqueueing the request to the engine.
 */
static int64_t
bdev_realstore_readv(struct realstore_bdev *rsbdev,
                     struct bdev_realstore_task *realstore_task,
                     struct iovec *iov, int iovcnt, uint64_t nbytes, uint64_t offset)
{
    assert(nbytes == iov[0].iov_len && iovcnt == 1);
    realstore_task->lcore = spdk_env_get_current_core();
    realstore_task->io_status = 0;

    SPDK_DEBUGLOG(SPDK_TRACE_REALSTORE, "read %d iovs size %lu to off: %#lx\n",
		  iovcnt, nbytes, offset);
    realstoreIscsiReadFile(rsbdev->fileid, /* FileId */
                        iov[0].iov_base, /* buf */
                        iov[0].iov_len,  /* size */
                        offset, /* offset */
                        realstore_task /* for IO completion */);

    return nbytes;
}

static void
bdev_io_complete_wrapper(void *arg1, void *arg2)
{
    struct bdev_realstore_task *realstore_task = arg1;
    int error_code;
    enum spdk_bdev_io_status status;

    error_code = realstore_task->io_status;

    if (error_code < 0) {
        status = SPDK_BDEV_IO_STATUS_FAILED;
    } else {
        status = SPDK_BDEV_IO_STATUS_SUCCESS;
    }

    spdk_bdev_io_complete(spdk_bdev_io_from_ctx(realstore_task), status);
}

void
bdev_realstore_io_complete(void *driver_ctx, int io_error_code);
/*
 * Function called by realstore once the I/O is completed.
 * Here, allocate and call an event to be run on the core where
 * the reactor for this target resides and executed the original
 * I/O request.
 */
void
bdev_realstore_io_complete(void *driver_ctx, int io_error_code)
{
    struct spdk_event *event;

    struct bdev_realstore_task *realstore_task = driver_ctx;
    realstore_task->io_status = io_error_code;

    uint32_t lcore = realstore_task->lcore;
    event = spdk_event_allocate(lcore, bdev_io_complete_wrapper, driver_ctx, NULL);

    spdk_event_call(event);

    return;
}
/*
 * Following prepares a write request to the underlying
 * realstore file engine. Returns immediately after
 * enqueueing the request to the engine.
 */
static int64_t
bdev_realstore_writev(struct realstore_bdev *rsbdev,
                     struct bdev_realstore_task *realstore_task,
                     struct iovec *iov, int iovcnt, size_t len, uint64_t offset)
{
    assert(len == iov[0].iov_len && iovcnt == 1);
    realstore_task->lcore = spdk_env_get_current_core();
    realstore_task->io_status = 0;

    SPDK_DEBUGLOG(SPDK_TRACE_REALSTORE, "write %d iovs size %lu from off: %#lx\n",
	      iovcnt, len, offset);
    realstoreIscsiWriteFile(rsbdev->fileid, /* FileId */
                        iov[0].iov_base, /* buf */
                        iov[0].iov_len,  /* size */
                        offset, /* offset */
                        realstore_task /* for IO completion */);

    return len;
}

static void
bdev_realstore_flush(struct realstore_bdev *rsbdev, struct bdev_realstore_task *realstore_task,
       uint64_t offset, uint64_t nbytes)
{
    return;
}

static void bdev_realstore_get_buf_cb(struct spdk_io_channel *ch, struct spdk_bdev_io *bdev_io)
{
    bdev_realstore_readv((struct realstore_bdev *)bdev_io->bdev->ctxt,
               (struct bdev_realstore_task *)bdev_io->driver_ctx,
               bdev_io->u.bdev.iovs,
               bdev_io->u.bdev.iovcnt,
               bdev_io->u.bdev.num_blocks * bdev_io->bdev->blocklen,
               bdev_io->u.bdev.offset_blocks * bdev_io->bdev->blocklen);
}

static void
bdev_realstore_reset(struct realstore_bdev *rsbdev, struct bdev_realstore_task *realstore_task)
{
}

static int _bdev_realstore_submit_request(struct spdk_bdev_io *bdev_io)
{
    switch (bdev_io->type) {
    case SPDK_BDEV_IO_TYPE_READ:
        spdk_bdev_io_get_buf(bdev_io, bdev_realstore_get_buf_cb,
                     bdev_io->u.bdev.num_blocks * bdev_io->bdev->blocklen);
	spdk_bdev_io_complete(bdev_io, SPDK_BDEV_IO_STATUS_SUCCESS);
        return 0;

    case SPDK_BDEV_IO_TYPE_WRITE:
        bdev_realstore_writev((struct realstore_bdev *)bdev_io->bdev->ctxt,
                (struct bdev_realstore_task *)bdev_io->driver_ctx,
                bdev_io->u.bdev.iovs,
                bdev_io->u.bdev.iovcnt,
                bdev_io->u.bdev.num_blocks * bdev_io->bdev->blocklen,
                bdev_io->u.bdev.offset_blocks * bdev_io->bdev->blocklen);
	spdk_bdev_io_complete(bdev_io, SPDK_BDEV_IO_STATUS_SUCCESS);
        return 0;
    case SPDK_BDEV_IO_TYPE_FLUSH:
        bdev_realstore_flush((struct realstore_bdev *)bdev_io->bdev->ctxt,
                   (struct bdev_realstore_task *)bdev_io->driver_ctx,
                   bdev_io->u.bdev.offset_blocks * bdev_io->bdev->blocklen,
                   bdev_io->u.bdev.num_blocks * bdev_io->bdev->blocklen);
	spdk_bdev_io_complete(bdev_io, SPDK_BDEV_IO_STATUS_SUCCESS);
        return 0;

    case SPDK_BDEV_IO_TYPE_RESET:
        bdev_realstore_reset((struct realstore_bdev *)bdev_io->bdev->ctxt,
                   (struct bdev_realstore_task *)bdev_io->driver_ctx);
	spdk_bdev_io_complete(bdev_io, SPDK_BDEV_IO_STATUS_SUCCESS);
        return 0;
    default:
	spdk_bdev_io_complete(bdev_io, SPDK_BDEV_IO_STATUS_FAILED);
        return 0;
    }
}
/*
 * Interface to bdev layer to submit IOs against this backend.
 */
static void bdev_realstore_submit_request(struct spdk_io_channel *ch, struct spdk_bdev_io *bdev_io)
{
    if (_bdev_realstore_submit_request(bdev_io) < 0) {
        spdk_bdev_io_complete(bdev_io, SPDK_BDEV_IO_STATUS_FAILED);
    }
}
/*
 * Runs a check against the type of IOs supported by
 * realstore backend.
 */
static bool
bdev_realstore_io_type_supported(void *ctx, enum spdk_bdev_io_type io_type)
{
    SPDK_NOTICELOG("bdev_realstore_io_type_supported io type = %d\n", (int)io_type);
    switch (io_type) {
    case SPDK_BDEV_IO_TYPE_READ:
    case SPDK_BDEV_IO_TYPE_WRITE:
    case SPDK_BDEV_IO_TYPE_FLUSH:
    case SPDK_BDEV_IO_TYPE_RESET:
        return true;

    default:
        return false;
    }
}
static int
bdev_realstore_dump_config_json(void *ctx, struct spdk_json_write_ctx *w)
{
    return 0;
}

static int
bdev_realstore_create_cb(void *io_device, void *ctx_buf)
{
    return 0;
}

static void
bdev_realstore_destroy_cb(void *io_device, void *ctx_buf)
{
}

static struct spdk_io_channel *
bdev_realstore_get_io_channel(void *ctx)
{
    struct realstore_bdev *rsbdev = ctx;

    return spdk_get_io_channel(&rsbdev->fileid);
}

static int
bdev_realstore_close(struct realstore_bdev *rsbdev)
{
    if (rsbdev->fileid == ~0ULL) {
        return 0;
    }

    // FIXME: realstoreFileDelete()?
    return 0;
}

static int
bdev_realstore_destruct(void *ctx)
{
    struct realstore_bdev *rsbdev = ctx;
    int rc = 0;

    TAILQ_REMOVE(&g_realstore_bdev_head, rsbdev, link);
    rc = bdev_realstore_close(rsbdev);
    if (rc < 0) {
        SPDK_ERRLOG("bdev_realstore_close() failed\n");
    }
    realstore_free_file(rsbdev);
    return rc;
}

static const struct spdk_bdev_fn_table realstore_fn_table = {
    .destruct           = bdev_realstore_destruct,
    .submit_request     = bdev_realstore_submit_request,
    .io_type_supported  = bdev_realstore_io_type_supported,
    .get_io_channel     = bdev_realstore_get_io_channel,
    .dump_config_json   = bdev_realstore_dump_config_json,
};

static void realstore_free_file(struct realstore_bdev *rsbdev)
{
    if (rsbdev == NULL)
        return;
    free(rsbdev->filename);
    free(rsbdev->bdev.name);
    free(rsbdev);
}

/*
 * Creates the bdev struct for this realstore backend corresponding
 * to this filename. Takes care to create the backing file.
 */
struct spdk_bdev *
create_realstore_file(const char *id, const char *filename, uint32_t block_size)
{
    struct realstore_bdev   *rsbdev;
    uint64_t file_size;

    rsbdev = calloc(sizeof(*rsbdev), 1);
    if (!rsbdev) {
        SPDK_ERRLOG("Unable to allocate enough memory for realstore backend\n");
        return NULL;
    }

    rsbdev->filename = strdup(filename);
    if (!rsbdev->filename) {
        SPDK_ERRLOG("Unable to create filename %s\n", filename);
        goto error_return;
    }

    if (bdev_realstore_create(rsbdev)) {
        SPDK_ERRLOG("Unable to create file %s. fileid: %" PRIu64 " errno: %d\n", filename, 
                     rsbdev->fileid, errno);
        goto error_return;
    }

    /* FIXME_ISCSI Retrieve file size */
    file_size = REALSTORE_DEFAULT_FILE_SIZE;

    rsbdev->bdev.name = strdup(id);
    if (!rsbdev->bdev.name) {
        goto error_return;
    }

    rsbdev->bdev.product_name = "Realstore file";
    rsbdev->bdev.module = SPDK_GET_BDEV_MODULE(realstore);

    /* FIXME_ISCSI Figure out implications ?! */
    rsbdev->bdev.write_cache = 1;

    if (block_size < 512) {
        SPDK_ERRLOG("Invalid block size %" PRIu32 " (must be at least 512).\n", block_size);
        goto error_return;
    }

    if (!spdk_u32_is_pow2(block_size)) {
        SPDK_ERRLOG("Invalid block size %" PRIu32 " (must be a power of 2.)\n", block_size);
        goto error_return;
    }

    rsbdev->bdev.blocklen = block_size;

    rsbdev->bdev.blockcnt = file_size / rsbdev->bdev.blocklen;
    rsbdev->bdev.ctxt = rsbdev;

    rsbdev->bdev.fn_table = &realstore_fn_table;

    spdk_io_device_register(&rsbdev->fileid, bdev_realstore_create_cb, bdev_realstore_destroy_cb, 0);
    spdk_bdev_register(&rsbdev->bdev);

    TAILQ_INSERT_TAIL(&g_realstore_bdev_head, rsbdev, link);
    return &rsbdev->bdev;

error_return:
    bdev_realstore_close(rsbdev);
    realstore_free_file(rsbdev);
    return NULL;
}

/*
 * Create the iscsi dir "iscsi/" which hosts all the backing files
static void
create_realstore_iscsi_parent_dir()
{
    realstoreIscsiCreateDir(REALSTORE_DIR_PATH);
}
 */

/*
 * Read the configs under the REALSTORE section and
 * initiatialize the realstore files that back the luns.
 */
static int
bdev_realstore_initialize(void)
{
    size_t i;
    struct spdk_conf_section *sp;
    struct spdk_bdev *bdev;

    SPDK_NOTICELOG("bdev_realstore_initialize \n");
    TAILQ_INIT(&g_realstore_bdev_head);
    sp = spdk_conf_find_section(NULL, "REALSTORE");
    if (!sp) {
        return 0;
    }

    i = 0;

    //create_realstore_iscsi_parent_dir();

    while (true) {
        const char *filename;   // Name of the backing file managed by realstore.
        const char *id;         // ID of this device that maps to a LUN.
        const char *block_size_str;
        uint32_t block_size = 0;

        filename = spdk_conf_section_get_nmval(sp, "REALSTORE", i, 0);

        if (!filename) {
            break;
        }

        id = spdk_conf_section_get_nmval(sp, "REALSTORE", i, 1);
        if (!id) {
            SPDK_ERRLOG("No identifier provided for realstore disk with file %s\n", filename);
            i++;
            continue;
        }

        block_size_str = spdk_conf_section_get_nmval(sp, "REALSTORE", i, 2);
        if (block_size_str) {
            block_size = atoi(block_size_str);
        }

        bdev = create_realstore_file(id, filename, block_size);
        if (!bdev) {
            SPDK_ERRLOG("Unable to create REALSTORE bdev from file %s\n", filename);
            i++;
            continue;
        }

        i++;
    }

    return 0;
}

static void
bdev_realstore_get_spdk_running_config(FILE *fp)
{
}

SPDK_LOG_REGISTER_TRACE_FLAG("realstore", SPDK_TRACE_REALSTORE)
